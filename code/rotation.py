import numpy as np
import pygame

SCREEN_SIZE = np.array((640, 480), dtype='int')


def draw_axis(screen, axis, label, color):
    center = SCREEN_SIZE // 2
    pygame.draw.line(screen, color, center, center + axis, 2)
    label_size = np.array(label.get_size(), dtype='int')
    screen.blit(label, center + axis - (label_size // 2))


def rotation_matrix(deg):
    rads = np.radians(deg)
    return np.array((
            (np.cos(rads), -1 * np.sin(rads)),
            (np.sin(rads), np.cos(rads))))


def run():
    pygame.init()
    screen = pygame.display.set_mode(size=SCREEN_SIZE)

    black = pygame.Color('black')
    white = pygame.Color('white')
    red = pygame.Color('red')
    blue = pygame.Color('blue')

    font = pygame.font.SysFont('monospace', 16, True)
    x_label = font.render('X', True, white)
    y_label = font.render('Y', True, white)

    x_axis = np.array((100, 0), dtype='int')
    y_axis = np.array((0, 100), dtype='int')

    rotation = 0
    while True:
        screen.fill(black)

        quit = False
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_q:
                    quit = True
                    break
                elif event.key == pygame.K_RIGHT:
                    rotation += 5
                    x_axis = rotation_matrix(5) @ x_axis
                    y_axis = rotation_matrix(5) @ y_axis
                elif event.key == pygame.K_LEFT:
                    rotation -= 5
                    x_axis = rotation_matrix(-5) @ x_axis
                    y_axis = rotation_matrix(-5) @ y_axis
        if quit:
            break

        draw_axis(screen, x_axis, x_label, red)
        draw_axis(screen, y_axis, y_label, blue)

        data = "Rotation: {}°, X: {}, Y: {}".format(
            rotation, x_axis.round(), y_axis.round())
        txt_surface = font.render(data, True, white)
        screen.blit(txt_surface, (5, 5))

        pygame.display.update()


if __name__ == '__main__':
    run()
