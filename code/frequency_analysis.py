import operator

theorical_frequencies = [
    8.4, 1.06, 3.03, 4.18, 17.26, 1.12, 1.27, 0.92, 7.34, 0.31, 0.05, 6.01,
    2.96, 7.13, 5.26, 3.01, 0.99, 6.55, 8.08, 7.07, 5.74, 1.32, 0.04, 0.45,
    0.3, 0.12]


def caesar(text, offset):
    new_letters = []
    for letter in text:
        new_letters.append(chr((ord(letter) - 65 + offset) % 26 + 65))
    return ''.join(new_letters)


def all_frequencies(subtext):
    l_subtext = len(subtext)
    freqs = []
    for i in range(26):
        counts = map(caesar(subtext, i).count, 'ABCDEFGHIJKLMNOPQRTSUVWXYZ')
        frequencies = [100 * c / l_subtext for c in counts]
        freq_i = sum(abs(offset_freq - theorical_frequencies[idx])
            for idx, offset_freq in enumerate(frequencies))
        freqs.append((i, freq_i))
    return freqs


def best_frequency(subtext):
    freqs = all_frequencies(subtext)
    return sorted(freqs, key=operator.itemgetter(1))[0]


# print(all_frequencies('LAPREMIEREPOSSIBILITEETANTDELOINLAPLUSPROBABLE'))
# print(best_frequency('MATMUEOI'))
# print(best_frequency('FHSFJTUF'))
# print(best_frequency('UGGUPQCP'))
# print(best_frequency('VUVTPSP'))
# print(best_frequency('LAPREMIEREPOSSIBILITEETANTDELOINLAPLUSPROBABLE'))
print(all_frequencies(caesar('LAPREMIEREPOSSIBILITEETANTDELOINLAPLUSPROBABLE', 5)))
# print(best_frequency(caesar('LAPREMIEREPOSSIBILITEETANTDELOINLAPLUSPROBABLE', 12)))
