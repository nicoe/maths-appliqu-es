class NotFoundError(LookupError):
    pass


class Node:

    def __init__(self):
        self.value = None
        self.left = None
        self.right = None

    def __str__(self):
        if self.value is None:
            return ""
        return "{} [{}] [{}]".format(
            self.value,
            self.left if self.left else "",
            self.right if self.right else "")

    @property
    def height(self):
        if self.value is None:
            return 0
        elif self.left and self.right:
            return 1 + max(self.left.height, self.right.height)
        elif self.left:
            return 1 + self.left.height
        else:
            return 1 + self.right.height

    @property
    def balance(self):
        if self.value is None:
            return 0
        return self.left.height - self.right.height

    @property
    def is_empty(self):
        return self.value is None

    def search(self, value):
        if value == self.value:
            return self
        elif value < self.value:
            if not self.left:
                raise NotFoundError
            return self.left.search(value)
        elif value > self.value:
            if not self.right:
                raise NotFoundError
            return self.right.search(value)

    def insert(self, value):
        if self.value is None:
            self.value = value
            self.left = Node()
            self.right = Node()
        elif value < self.value:
            self.left = self.left.insert(value)
        elif value > self.value:
            self.right = self.right.insert(value)

        balance = self.balance
        if abs(balance) > 1:
            if balance > 0 and value < self.left.value:
                return self.right_rotate()
            elif balance < 0 and value > self.right.value:
                return self.left_rotate()
            elif balance > 0 and value > self.left.value:
                self.left = self.left.left_rotate()
                return self.right_rotate()
            elif balance < 0 and value < self.right.value:
                self.right = self.right.right_rotate()
                return self.left_rotate()

        return self

    def delete(self, value):
        if self.value is None:
            return self
        elif value < self.value:
            self.left = self.left.delete(value)
        elif value > self.value:
            self.right = self.right.delete(value)
        else:
            if self.left.is_empty:
                return self.right
            elif self.right.is_empty:
                return self.left
            else:
                new_root = self.left.get_max()
                self.value = new_root.value
                self.left = self.left.delete(new_root.value)

        balance = self.balance
        if abs(balance) > 1:
            left_balance = self.left.balance
            right_balance = self.right.balance
            if balance > 0 and left_balance >= 0:
                return self.right_rotate()
            elif balance < 0 and right_balance <= 0:
                return self.left_rotate()
            elif balance > 0 and left_balance < 0:
                self.left = self.left.left_rotate()
                return self.right_rotate()
            elif balance < 0 and right_balance > 0:
                self.right = self.right.right_rotate()
                return self.left_rotate()

        return self

    def left_rotate(self):
        y = self.right
        tree = y.left

        y.left = self
        self.right = tree

        return y

    def right_rotate(self):
        y = self.left
        tree = y.right

        y.right = self
        self.left = tree

        return y

    def get_max(self):
        if self.right.is_empty or self.right.value is None:
            return self
        return self.right.get_max()

    def order_asc(self):
        if not self.left.is_empty:
            self.left.order_asc()
        print(self.value)
        if not self.right.is_empty:
            self.right.order_asc()


if __name__ == '__main__':
    t = Node()
    t = t.insert(10)
    t = t.insert(20)
    t = t.insert(30)
    print(t)
    t = t.insert(40)
    t = t.insert(50)
    t = t.insert(25)
    print(t)
    t.order_asc()

    t = t.delete(40)
    print(t)
    t = t.delete(50)
    print(t)

    t.order_asc()
