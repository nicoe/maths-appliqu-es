import numpy as np
import pygame

SCREEN_SIZE = np.array((640, 480), dtype='int')


def homogeneous2D(coord):
    return np.array((coord[0], coord[1]))


def draw_axis(screen, center, axis, label, color):
    pygame.draw.line(screen, color, homogeneous2D(center), homogeneous2D(axis), 2)
    label_size = np.array(label.get_size(), dtype='int')
    screen.blit(label, homogeneous2D(axis) - (label_size // 2))


def rotation_matrix(center, deg):
    rads = np.radians(deg)
    to_origin = translation(-center[0], -center[1])
    rotation = np.array((
            (np.cos(rads), -1 * np.sin(rads), 0),
            (np.sin(rads), np.cos(rads), 0),
            (0, 0, 1)
            ))
    back_in_place = translation(center[0], center[1])
    return back_in_place @ rotation @ to_origin


def translation(tx, ty):
    return np.array((
            (1, 0, tx),
            (0, 1, ty),
            (0, 0, 1)
            ))


def run():
    pygame.init()
    screen = pygame.display.set_mode(size=SCREEN_SIZE)

    black = pygame.Color('black')
    white = pygame.Color('white')
    red = pygame.Color('red')
    blue = pygame.Color('blue')

    font = pygame.font.SysFont('monospace', 16, True)
    x_label = font.render('X', True, white)
    y_label = font.render('Y', True, white)

    center = np.array((SCREEN_SIZE[0] // 2, SCREEN_SIZE[1] // 2, 1), dtype='int')
    x_axis = center + np.array((100, 0, 0), dtype='int')
    y_axis = center + np.array((0, 100, 0), dtype='int')

    rotation = 0
    while True:
        screen.fill(black)

        quit = False
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_q:
                    quit = True
                    break
                elif event.key == pygame.K_a:
                    rotation += 5
                    r = rotation_matrix(center, 5)
                    x_axis = r @ x_axis
                    y_axis = r @ y_axis
                elif event.key == pygame.K_e:
                    rotation -= 5
                    r = rotation_matrix(center, -5)
                    x_axis = r @ x_axis
                    y_axis = r @ y_axis
                elif event.key == pygame.K_LEFT:
                    t = translation(-5, 0)
                    center = t @ center
                    x_axis = t @ x_axis
                    y_axis = t @ y_axis
                elif event.key == pygame.K_RIGHT:
                    t = translation(-5, 0)
                    center = t @ center
                    x_axis = t @ x_axis
                    y_axis = t @ y_axis
                elif event.key == pygame.K_UP:
                    t = translation(0, -5)
                    center = t @ center
                    x_axis = t @ x_axis
                    y_axis = t @ y_axis
                elif event.key == pygame.K_LEFT:
                    t = translation(0, 5)
                    center = t @ center
                    x_axis = t @ x_axis
                    y_axis = t @ y_axis
        if quit:
            break

        draw_axis(screen, center, x_axis, x_label, red)
        draw_axis(screen, center, y_axis, y_label, blue)

        data = "Rotation: {}°, X: {}, Y: {}".format(
            rotation, x_axis.round(), y_axis.round())
        txt_surface = font.render(data, True, white)
        screen.blit(txt_surface, (5, 5))

        pygame.display.update()


if __name__ == '__main__':
    run()
